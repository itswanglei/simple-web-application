package com.twuc.simplewebapplication.domian;

import com.twuc.simplewebapplication.contract.CreateProductRequest;

import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "products")
@Entity
public class Product {

    @Id
    @Column(name = "productCode", nullable = false, length = 15)
    private String code;

    @Column(name = "productName", nullable = false, length = 70)
    private String name;

    @ManyToOne
    @JoinColumn(name = "productLine")
    private ProductLine productLine;

    @Column(name = "productScale", nullable = false, length = 10)
    private String scale;

    @Column(name = "productVendor", nullable = false, length = 50)
    private String vendor;

    @Column(name = "productDescription", nullable = false, columnDefinition = "text")
    private String description;

    @Column(name = "quantityInStock", nullable = false)
    private Short quantityInStock;

    @Column(name = "buyPrice", nullable = false)
    private BigDecimal buyPrice;

    @Column(name = "MSRP", nullable = false)
    private BigDecimal MSRP;

    public Product() {
    }

    public Product(CreateProductRequest createProductRequest, ProductLine productLine) {
        this.code = createProductRequest.getCode();
        this.name = createProductRequest.getName();
        this.productLine = productLine;
        this.scale = createProductRequest.getScale();
        this.vendor = createProductRequest.getVendor();
        this.description = createProductRequest.getDescription();
        this.quantityInStock = createProductRequest.getQuantityInStock();
        this.buyPrice = createProductRequest.getBuyPrice();
        this.MSRP = createProductRequest.getMSRP();
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public ProductLine getProductLine() {
        return productLine;
    }

    public String getScale() {
        return scale;
    }

    public String getVendor() {
        return vendor;
    }

    public String getDescription() {
        return description;
    }

    public Short getQuantityInStock() {
        return quantityInStock;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public BigDecimal getMSRP() {
        return MSRP;
    }
}


