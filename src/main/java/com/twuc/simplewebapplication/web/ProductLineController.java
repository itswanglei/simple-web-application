package com.twuc.simplewebapplication.web;

import com.twuc.simplewebapplication.contract.ProductLineResourceAssembler;
import com.twuc.simplewebapplication.domian.ProductLine;
import com.twuc.simplewebapplication.service.ProductLineService;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api")
public class ProductLineController {

    private ProductLineService service;
    private ProductLineResourceAssembler assembler;

    public ProductLineController(ProductLineService service, ProductLineResourceAssembler assembler) {
        this.service = service;
        this.assembler = assembler;
    }

    @GetMapping("/product-lines")
    public Resources<Resource<ProductLine>> getAll(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String orderBy,
            @RequestParam(required = false) String direction
    ) {
        List<Resource<ProductLine>> resources = service.getAll(page, size, orderBy, direction);
        return new Resources<>(resources,
                linkTo(methodOn(ProductLineController.class).getAll(page, size, orderBy, direction)).withSelfRel());
    }

    @GetMapping("/product-lines/{name}")
    public Resource<ProductLine> getOne(@PathVariable String name) {
        return assembler.toResource(service.getOne(name));
    }

    @PostMapping("/product-lines")
    public ResponseEntity createProductLine(@RequestBody @Valid ProductLine productLine) {
        service.create(productLine);
        return ResponseEntity
                .created(linkTo(methodOn(ProductLineController.class).getOne(productLine.getName())).toUri())
                .build();
    }
}
