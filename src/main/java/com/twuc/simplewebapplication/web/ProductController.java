package com.twuc.simplewebapplication.web;

import com.twuc.simplewebapplication.contract.CreateProductRequest;
import com.twuc.simplewebapplication.contract.GetProductResponse;
import com.twuc.simplewebapplication.domian.Product;
import com.twuc.simplewebapplication.service.ProductService;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api")
public class ProductController {

    private ProductService service;

    public ProductController(ProductService service) {
        this.service = service;
    }

    @GetMapping("/products")
    public Resources getAll(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String orderBy,
            @RequestParam(required = false) String direction
    ) {
        List<Resource<GetProductResponse>> resources = service.getAll(page, size, orderBy, direction);
        return new Resources(resources,
                linkTo(methodOn(ProductController.class).getAll(page, size, orderBy, direction)).withSelfRel());
    }

    @GetMapping("/products/{productCode}")
    public GetProductResponse getOne(@PathVariable String productCode) {
        Product product = service.getOne(productCode);
        return new GetProductResponse(product);
    }

    @PostMapping("/products")
    public ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest createProductRequest) {
        service.create(createProductRequest);
        return ResponseEntity
                .created(linkTo(methodOn(ProductController.class).getOne(createProductRequest.getCode())).toUri())
                .build();
    }
}
