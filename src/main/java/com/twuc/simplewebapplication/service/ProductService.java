package com.twuc.simplewebapplication.service;

import com.twuc.simplewebapplication.contract.CreateProductRequest;
import com.twuc.simplewebapplication.contract.GetProductResponse;
import com.twuc.simplewebapplication.contract.ProductResourceAssembler;
import com.twuc.simplewebapplication.dao.ProductRepository;
import com.twuc.simplewebapplication.domian.Product;
import com.twuc.simplewebapplication.domian.ProductLine;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ProductService {

    private ProductRepository repository;
    private ProductResourceAssembler assembler;
    private ProductLineService productLineService;

    public ProductService(
            ProductRepository repository,
            ProductResourceAssembler assembler,
            ProductLineService productLineService) {
        this.repository = repository;
        this.assembler = assembler;
        this.productLineService = productLineService;
    }

    public List<Resource<GetProductResponse>> getAll() {
        return repository.findAll()
                .stream()
                .map(product -> assembler.toResource(new GetProductResponse(product)))
                .collect(Collectors.toList());
    }

    public List<Resource<GetProductResponse>> getAll(Integer page, Integer size, String orderBy, String direction) {
        String defaultOrderBy = orderBy == null ? "code" : orderBy;
        Sort.Direction defaultDirection = direction == null ? Sort.Direction.ASC : Sort.Direction.DESC;
        Sort sort = new Sort(defaultDirection, defaultOrderBy);

        if (page != null && size != null) {
            return this.findAll(page, size, sort);
        } else {
            return this.findAll(sort);
        }
    }

    private List<Resource<GetProductResponse>> findAll(Sort sort) {
        Stream<Product> stream = repository.findAll(sort).stream();
        return this.productStreamToResourceList(stream);
    }

    private List<Resource<GetProductResponse>> findAll(Integer page, Integer size, Sort sort) {
        Stream<Product> stream = repository.findAll(PageRequest.of(page, size, sort)).stream();
        return this.productStreamToResourceList(stream);
    }

    private List<Resource<GetProductResponse>> productStreamToResourceList(Stream<Product> stream) {
        return stream.map(product -> assembler.toResource(new GetProductResponse(product)))
                .collect(Collectors.toList());
    }

    public Product getOne(String productCode) {
        return repository.findById(productCode).orElseThrow(NoSuchElementException::new);
    }

    public void create(CreateProductRequest createProductRequest) {
        ProductLine productLine = productLineService.getOne(createProductRequest.getProductLine());
        Product product = new Product(createProductRequest, productLine);
        repository.save(product);
    }
}
