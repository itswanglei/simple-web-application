package com.twuc.simplewebapplication.contract;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.twuc.simplewebapplication.domian.Product;

import java.math.BigDecimal;

public class GetProductResponse {

    private String code;
    private String name;
    private String productLine;
    private String scale;
    private String vendor;
    private String description;
    private Short quantityInStock;
    private BigDecimal buyPrice;
    @JsonProperty
    private BigDecimal MSRP;

    public GetProductResponse(Product product) {
        this.code = product.getCode();
        this.name = product.getName();
        this.productLine = product.getProductLine().getName();
        this.scale = product.getScale();
        this.vendor = product.getVendor();
        this.description = product.getDescription();
        this.quantityInStock = product.getQuantityInStock();
        this.buyPrice = product.getBuyPrice();
        this.MSRP = product.getMSRP();
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getProductLine() {
        return productLine;
    }

    public String getScale() {
        return scale;
    }

    public String getVendor() {
        return vendor;
    }

    public String getDescription() {
        return description;
    }

    public Short getQuantityInStock() {
        return quantityInStock;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    @JsonIgnore
    public BigDecimal getMSRP() {
        return MSRP;
    }
}
