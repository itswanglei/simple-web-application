package com.twuc.simplewebapplication.contract;

import com.twuc.simplewebapplication.domian.ProductLine;
import com.twuc.simplewebapplication.web.ProductLineController;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ProductLineResourceAssembler implements ResourceAssembler<ProductLine, Resource<ProductLine>> {
    @Override
    public Resource<ProductLine> toResource(ProductLine entity) {
        return new Resource<>(entity,
                linkTo(methodOn(ProductLineController.class).getOne(entity.getName())).withSelfRel());
    }
}
