package com.twuc.simplewebapplication.contract;

import com.twuc.simplewebapplication.domian.Product;
import com.twuc.simplewebapplication.web.ProductController;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ProductResourceAssembler implements ResourceAssembler<GetProductResponse, Resource<GetProductResponse>> {
    @Override
    public Resource<GetProductResponse> toResource(GetProductResponse entity) {
        return new Resource<>(entity,
                linkTo(methodOn(ProductController.class).getOne(entity.getCode())).withSelfRel());
    }
}
