package com.twuc.simplewebapplication.contract;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

@ControllerAdvice
public class ErrorHandleAdvice extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = {NoSuchElementException.class})
    public ResponseEntity handler() {
        return ResponseEntity.notFound().build();
    }
}
